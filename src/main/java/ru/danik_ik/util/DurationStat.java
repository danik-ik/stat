package ru.danik_ik.util;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

/**
 * Designed to collect statistics on execution time in the context of a certain set of keys. <br/>
 * WARNING: NOT thread-safe! <br/>
 * NOTE: Nested intervals are excluded from enclosing intervals. Thus, it is safe to make nested calls.<br/>
 * NOTE: The collected statistics include the idle time of the thread. Thus, the competition of threads
 * for CPU time can significantly reduce the reliability of the collected data.<br/>
 * Usage:<br/>
 * <pre>
 * DurationStat&lt;MyEnum&gt; it = new DurationStat&lt;&gt;();
 *
 * it.start(MyEnum.SOME_KEY);
 * try {
 *     doSomething();
 *     it.start(MyEnum.OTHER_KEY);
 *     try {
 *         doSomethingOther();
 *     } finally {
 *         it.finish();
 *     }
 * } finally {
 *     it.finish();
 * }
 *
 * SomeResultType result = it.apply(
 *     MyEnum.CALCULATE,
 *     this::calculateSomething()
 * );
 * </pre>
 * @param <T> The key in the context of which the statistics are collected
 */
public class DurationStat<T> {
    private final Deque<T> stack = new ArrayDeque<>();
    private Instant prev;
    private final Map<T, Duration> accumulator = new HashMap<>();
    private final Supplier<Instant> timestampSupplier;

    /**
     * For test purpose
     * @param timestampSupplier mock timestamp supplier
     */
    DurationStat(Supplier<Instant> timestampSupplier) {
        this.timestampSupplier = timestampSupplier;
    }

    public DurationStat() {
        this.timestampSupplier = Instant::now;
    }

    /**
     * Opens the timeslot for the passed key. Pauses the current interval if it exists
     * @param key key
     * @return current timestamp
     */
    public Instant start(T key) {
        final Instant now = storeDuration();
        stack.push(key);
        return now;
    }

    /**
     * Ends the current time interval. Restores a previously opened one, if it exists
     * @return current timestamp
     */
    public Instant finish() {
        final Instant now = storeDuration();
        stack.pop();
        return now;
    }

    /**
     * @return summary statistics as a Map
     */
    public Map<T, Duration> getStat() {
        return Collections.unmodifiableMap(accumulator);
    }

    public <R> R applyWithCheckedException(T key, Callable<R> action) throws Exception {
        final R result;
        start(key);
        try {
            result = action.call();
        } finally {
             finish();
        }
        return result;
    }

    public <R> R apply(T key, Callable<R> action) {
        try {
            return applyWithCheckedException(key, action);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void applyWithCheckedException(T key, RunnableWithException action) throws Exception {
        applyWithCheckedException(key, () -> {
            action.run();
            return null;
        });
    }

    public void apply(T key, Runnable action) {
        try {
            applyWithCheckedException(key, () -> {
                action.run();
                return null;
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Instant storeDuration() {
        T currentKey = stack.peek();
        Instant now = timestampSupplier.get();
        if (currentKey != null) {
            accumulate(currentKey, Duration.between(prev, now));
        }
        prev = now;
        return now;
    }

    private void accumulate(T currentKey, Duration duration) {
        accumulator.merge(currentKey, duration, Duration::plus);
    }

    @FunctionalInterface
    public interface RunnableWithException {
        void run() throws Exception;
    }
}
