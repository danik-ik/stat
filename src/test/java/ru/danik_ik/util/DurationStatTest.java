package ru.danik_ik.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.currentTimeMillis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DurationStatTest {
    public static final int RECURSION_COUNTER = 1000;
    public static final int DELAY_A = 3;
    public static final int DELAY_B = 2;
    public static final int DELAY_C = 4;
    private final MockTimestampSource mockTimestampSource = new MockTimestampSource();
    private DurationStat<MethodRef> stat;

    @BeforeEach
    public void setup() {
        stat = new DurationStat<>(mockTimestampSource::get);
    }

    @Test
    public void CheckTimeConsistenceForRecursiveCalls() {
        Instant a = stat.start(MethodRef.WRAPPER);
        Instant b;
        try {
            a(stat, new CountDown(RECURSION_COUNTER));
        } finally {
            b = stat.finish();
        }

        Duration directlyMeasuredDuration = Duration.between(a, b);

        final Map<MethodRef, Duration> statResult = stat.getStat();
        final Duration calculatedDuration = statResult.values().stream().reduce(Duration.ZERO, Duration::plus);

        assertEquals(
            directlyMeasuredDuration,
            calculatedDuration,
            "sum of durations in statistics should be strictly equal to directly measured one"
        );
        assertEquals(
            0,
            statResult.get(MethodRef.WRAPPER).toMillis(),
            "The accumulated durations for WRAPPER"
        );
        assertEquals(
            RECURSION_COUNTER * DELAY_A,
            statResult.get(MethodRef.A).toMillis(),
            "The accumulated durations for A"
        );
        assertEquals(
            RECURSION_COUNTER * DELAY_B,
            statResult.get(MethodRef.B).toMillis(),
            "The accumulated durations for B"
        );
        assertEquals(
            RECURSION_COUNTER * DELAY_C,
            statResult.get(MethodRef.C).toMillis(),
            "The accumulated durations for C"
        );
    }

    @Test
    public void SequentialTest() {
        stat.apply(MethodRef.WRAPPER, () -> sleep(100));
        sleep(300);
        stat.apply(MethodRef.WRAPPER, () -> sleep(100));

        final Map<MethodRef, Duration> statResult = stat.getStat();
        Duration duration = statResult.get(MethodRef.WRAPPER);

        assertEquals(200, duration.toMillis());
    }

    @Test
    public void testApplyForCallable() {
        final Integer applyResult = stat.apply(MethodRef.WRAPPER, () -> {
            sleep(100);
            return 12345;
        });
        assertEquals(12345, applyResult);
    }

    @Test
    public void testApplyForCallableWithExceptionSuccess() throws Exception {
        final boolean success = true;
        final Integer applyResult = stat.applyWithCheckedException(MethodRef.WRAPPER, () -> {
            if (success) {
                return 12345;
            } else {
                throw new TestException("54321");
            }
        });
        assertEquals(12345, applyResult);
    }

    @Test
    public void testApplyForCallableWithExceptionTrows() {
        TestException thrown = assertThrows(
            TestException.class,
            () -> {
                final boolean success = false;
                stat.applyWithCheckedException(MethodRef.WRAPPER, () -> {
                    if (success) {
                        return 12345;
                    } else {
                        throw new TestException("54321");
                    }
                });
            }
        );
        assertEquals("54321", thrown.getMessage());
    }

    @Test
    public void testApplyForRunnableWithExceptionSuccess() throws Exception {
        final boolean success = true;
        AtomicInteger mutable = new AtomicInteger(0);
        stat.applyWithCheckedException(MethodRef.WRAPPER, () -> {
            if (success) {
                mutable.set(12345);
            } else {
                throw new TestException("54321");
            }
        });
        assertEquals(12345, mutable.get());
    }

    @Test
    public void testApplyForRunnableWithExceptionTrows() {
        TestException thrown = assertThrows(
            TestException.class,
            () -> {
                final boolean success = false;
                stat.applyWithCheckedException(MethodRef.WRAPPER, () -> {
                    if (success) {
                        return;
                    } else {
                        throw new TestException("54321");
                    }
                });
            }
        );
        assertEquals("54321", thrown.getMessage());
    }

    @Test
    public void testDefaultConstructor() throws InterruptedException {
        final Instant a = Instant.now();
        Thread.sleep(1);
        final Instant start = new DurationStat().start(this);
        Thread.sleep(1);
        final Instant b = Instant.now();
        assertNotNull(start);
        assertNotEquals(0, start.toEpochMilli());
        assertTrue(a.compareTo(start) < 0);
        assertTrue(start.compareTo(b) < 0);
    }

    private void a(DurationStat<MethodRef> stat, CountDown countDown) {
        if (countDown.decrement() <= 0) return;
        stat.start(MethodRef.A);
        try {
            sleep(DELAY_A);
            b(stat, countDown);
        } finally {
            stat.finish();
        }
    }

    private void b(DurationStat<MethodRef> stat, CountDown countDown) {
        stat.start(MethodRef.B);
        try {
            sleep(DELAY_B);
            c(stat, countDown);
        } finally {
            stat.finish();
        }
    }

    private void c(DurationStat<MethodRef> stat, CountDown countDown) {
        stat.start(MethodRef.C);
        try {
            a(stat, countDown);
            sleep(DELAY_C);
        } finally {
            stat.finish();
        }
    }

    private void sleep(int millis) {
        mockTimestampSource.sleep(millis);
    }

    private enum MethodRef {
        WRAPPER, A, B, C
    }

    private static class CountDown {
        private int value;

        public CountDown(int value) {
            this.value = value;
        }

        private int decrement() {
            return value --;
        }
    }

    private static class MockTimestampSource {
        long timestamp = currentTimeMillis();

        void sleep(long millis) {
            timestamp += millis;
        }

        Instant get() {
            return Instant.ofEpochMilli(timestamp);
        }
    }

    public static class TestException extends Exception {
        public TestException(String message) {
            super(message);
        }
    }
}
